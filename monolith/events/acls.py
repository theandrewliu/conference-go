from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests


def get_photo(location):
    url = "https://api.pexels.com/v1/search?query=" + location
    headers = {"Authorization": PEXELS_API_KEY}

    # use request to get a photo
    r = requests.get(url, headers=headers)

    r = json.loads(r.content)
    photos = r["photos"]
    if photos and len(photos) > 0:
        return photos


def get_weather(city, state):
    url = (
        "http://api.openweathermap.org/geo/1.0/direct?q="
        + city
        + ","
        + state
        + "&appid="
        + OPEN_WEATHER_API_KEY
    )
    headers = {"Authorization": OPEN_WEATHER_API_KEY}
    r = requests.get(url, headers=headers)

    r = json.loads(r.content)[0]
    lat = str(r["lat"])
    lon = str(r["lon"])

    url = (
        "https://api.openweathermap.org/data/2.5/weather?lat="
        + lat
        + "&lon="
        + lon
        + "&appid="
        + OPEN_WEATHER_API_KEY
    )
    r = requests.get(url, headers=headers)
    r = json.loads(r.content)
    temp = r["main"]["temp"]
    temp = 1.8 * (int(temp) - 273.15) + 32
    desc = r["weather"][0]["description"]
    weather = {"temperature": int(temp), "description": desc}
    return weather
